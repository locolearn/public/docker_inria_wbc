# RUNNING DOCKER ON WINDOWS

## INSTALL WSL
- open powershell

- dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all
- dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all

- download and install (for x64) : https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

- wsl --set-default-version 2

- install ubuntu or ubuntu 20.04 from windows store

## INSTALL X11 SERVER
- go to : https://sourceforge.net/projects/vcxsrv/
- download the latest version
- execute Xlaunch and select multiple windows 
- choose start no client option
- uncheck native opengl optin and check disable access control
- keep xlaunch open for next steps

## INSTALL DOCKER

- Launch ubuntu
- execute the following command to install docker :
- curl -sSL https://get.docker.com | sh
- sudo usermod -aG docker $USER
- sudo dockerd

## SET & TEST X11 forwarding
- execute the following command to set properly the display :
- export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"

- (test the x11 forwarding)
- install x11-apps : sudo apt install x11-apps
- execute: xcalc
- if everything is properly set you should see the calculator on your screen

- execute the following command :
- export LIBGL_ALWAYS_INDIRECT=0

## DOWNLOAD AND USE THE INRIA_WBC DOCKER IMAGE
Follow the documentation [here](../README.md) to pull the image and run your container

In the container redo all step in #SET & TEST X11 forwarding


## TROUBLESHOOTING

Check the troubleshooting section [here](../README.md)

