# DOCKER_INRIA_WBC

This docker repository corresponds to [this](https://github.com/resibots/inria_wbc.git) installation

*The PAL version uses the private distribution of ROS from PAL Robotics (which is **not** open source). It should not be distributed outside of the team and it should only be used by the developpers that are using the Talos robot.*

## Examples
### Pulling (automatic if you do not have the image)
- `docker login gitlab.inria.fr`
- `docker pull <image_name>`: `docker pull registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_icub:latest`

### Running 
- Nvidia docker (2022.01): `./run_docker.sh --no-graphics -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_nvidia:2022.02 -c /bin/bash`
- Nvidia docker (2022.01-light): `./run_docker.sh --no-graphics -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_nvidia:2022.02-light -c /bin/bash`
- PAL docker (latest): `./run_docker.sh --no-graphics -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_pal:latest -c /bin/bash`
- iCub: `./run_docker.sh --user icub -w /home/icub -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_icub:latest  terminator`

**By default, you should use the `inria_wbc_nvidia:2022.01-light` image.**

## FOR WINDOWS

Read the documentation [here](docs/windows.md)

*Please note that we advise you to use a Linux computer*

## Available images
Images are available in the left bar, item "Packages and repository".

### Nomenclature:
- `-light` means that only the minimum number of packages is installed (faster to download, easier to understand)
- without the `light` tag, we install most of our packages (sferes2, limbo, teleoperation, ...). This can be overwhelming.
- `nvidia` has graphical support if you have a nvidia graphic card (works also on intel cards)
- `open` is a standard docker (not nvidia)
- `pal` includes the proprietary software from PAL robotics & ROS (required for Talos operation)
- `icub`includes the superbuild (in `/usr/local/src/robot`) and Gazebo11
- `2022.02`: release number (date.month)

### WARNING: Pal images are in a different (private) container registry:
- for open-source images: `registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/`
- for PAL images: `registry.gitlab.inria.fr/locolearn/docker_talos/inria_wbc_pal`

### Release:
#### 2022.02 (current)
For laptops and most use cases:
- `inria_wbc_nvidia:2022.02`
- `inria_wbc_nvidia:2022.02-light`

For other computers (servers):
- `inria_wbc_open:2022.02`
- `inria_wbc_open:2022.02-light`

For Talos (closed source):
- `inria_wbc_pal:2022.02`
- `inria_wbc_pal:2022.02-light`

For iCub (inc. nvidia support nvidia):
- `inria_wbc_icub:2022.02`
- `inria_wbc_icub:2022.02-light`

#### 2021.04 (old)
- `inria_wbc_nvidia:2021.04`
- `inria_wbc_nvidia:2021.04-light`
- `inria_wbc_open:2021.04`
- `inria_wbc_open:2021.04-light`
- `inria_wbc_pal:2021.04`
- `inria_wbc_pal:2021.04-light`

### Development images (weekly master)
- `inria_wbc_nvidia:latest`
- `inria_wbc_nvidia:latest-light`
- `inria_wbc_open:latest`
- `inria_wbc_open:latest-light`
- `inria_wbc_pal:latest`
- `inria_wbc_pal:latest-light`


## Running the docker

```
git clone https://gitlab.inria.fr/locolearn/public/docker_inria_wbc.git
```

### Options of `run_docker.sh`
```shell

usage: ./run_docker.sh [--no-graphics] [-it] your_image [-c your_command]
   -it: interactive
   default command is /bin/bash [the docker entry point]

```




### Standard (open-source container / non-NVIDIA / non-ROS / non-PAL) :
```
cd docker_inria_wbc

docker login registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc
docker pull registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_open:latest
./run_docker.sh -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_open:latest -c terminator

cd robot_dart
./build/talos

cd ~/inria_wbc/build
./test_controller_graphics
./test_controller_graphics ../etc/arm.yaml
 ```


### Standard NVIDIA (open-source container / for NVIDIA / non-ROS / non-PAL) :
```
cd docker_inria_wbc

docker login registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc
docker pull registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_nvidia:latest
./run_docker.sh -it registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_nvidia:latest -c /bin/bash

cd robot_dart
./build/talos

cd ~/inria_wbc/build
./talos_graphics
./talos_graphics ../etc/arm.yaml
 ```

### PAL image for Talos (requires Inria access)
```
cd docker_inria_wbc

docker login registry.gitlab.inria.fr/locolearn/public/docker_talos
docker pull  registry.gitlab.inria.fr/locolearn/docker_talos/inria_wbc_pal:latest

./run_docker.sh -it registry.gitlab.inria.fr/locolearn/docker_talos/inria_wbc_pal:latest -c /bin/bash

cd robot_dart
./build/talos

cd ~/inria_wbc/build
./talos_graphics
./talos_graphics ../etc/arm.yaml
 ```

## Editing files in the docker (& working)
VS Code provides a nice solution with the Docker module and the Remote package
- install the `Docker` extension by Microsoft
- once installed, click on the whale icon in the vertical bar (on the left)
- you should see the containers that are running: right-click on the name, then choose `attach visual studio code`; please note that you can also attach a shell
- you can also use the same GUI to start containers, etc.

## Editing in a remote docker (e.g. the cluster is running on a distant computer like a cluster)
In VS Code, install `Remote - Containers` by Microsoft (you need the Docker extension too).
- in a terminal, create a "docker context", which is essentially a way to tell docker that your Docker Daemon is running on a different computer:
```
docker context create evo256-docker --docker "host=ssh://jmouret@evo256.loria.fr" 
```
- Do not forget to replace the name of the computer (here `evo256.loria.fr`) and your login (here `jmouret`)
- In VS Code, open the command palette (control-shift-p) and select `Docker Contexts: Use`, then choose the context that you just created (in the example, `evo256-docker`)
- Now your remote images should appear exactly like your local images (see the previous sections); in particular, you can attach VS Code and attach a shell... which will all be running on the remote computer
- on Mac, you need AT EVERY LOGIN to add your keys: `ssh-add ~/.ssh/id_rsa`
(see https://code.visualstudio.com/docs/containers/ssh)

## Building the docker (developers only)

To build everything run :
- `python make.py -b all --tag 2022.month.day`

Versions:
- nvidia
- open
- icub
- pal
- tiago


Otherwise you can :

- `python make.py --h`: print help

Some examples :

- `python make.py -b open` : build the open-source image (no PAL dependency and no private repo and no Nvidia)
- `python make.py -b nvidia`: build the open-source image (no PAL dependency and no private repo and for Nvidia)
- `python make.py -b pal`: build from the PAL docker
- `python make.py --build open --lightweight --tag 2021.03 --verbose` 
- `python make.py --push all`
- `python make.py --build all`

### Useful pptions:
- `-w` : do not do all the git clone (which takes time)
- `-n` : no cache (like a make clean)



# Note about the policy for installation
*The objective is to present a new user with just the relevant code (to avoid to lose him/her)*.

- the dependencies that are not packaged are in `$HOME/deps` and are installed system-wide with `sudo make install` (they are cloned with --depth 1 to save space). Examples: DART, Magnum, Assimp
- The 'development code' (from us and our collaborators) is in `$HOME` and installed in `$HOME/install`; this is the code that we might modify or use specific branches. Examples: TSID, Pinocchio, RobotDART


# Troubleshooting

## If you have problem restarting your container on computer reboot

```
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $containerId`
docker start $containerId
```

## Factory error

This should change with a robot_dart update but right now an error occurs on the controller factory
- Go inside the folder ```/home/pal/install/lib/cmake/Robotdart/RobotDart.configcmake```
- Find the line where you have "fexplicit = hidden" 
- Remove it
- Compile inria_wbc again with ```sudo make install```

# Visual code config
You can use visual code to work on your docker

Install the following apps:
- docker
- Remote Development
- Remote X11 (optional)
- Clang-Format
