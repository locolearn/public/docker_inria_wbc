#!/usr/bin/python3

import argparse
import subprocess
import sys,os


def parse_args():
    # setup arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("-b", '--build',help='open or nvidia or pal or all')
    parser.add_argument("-d", '--dryrun', action="store_true", help='print the commands to be executed, but do not run them')
    parser.add_argument("-l", '--lightweight', action="store_true", help='lightweight version')    
    parser.add_argument("-n", "--no_cache", help="do not use any cache (clean)", action="store_true")
    parser.add_argument("-p", '--push', help='open or nvidia or pal')
    parser.add_argument("-t", '--tag', default="latest", help='tag')
    parser.add_argument("-v", '--verbose', action="store_true", help='verbose')
    parser.add_argument("-w", '--without_prep_cmds', action="store_true", help='without_preparation_commands avoids to clone again the repos')
    parser.add_argument("-r", '--registry', action="store_true", help='list the registry URLs')
    parser.add_argument("-a", '--architecture', default="", help='compile for a specific architecture, e.g.: ivybridge (talos), native or broadwell [default:""]')

    args = parser.parse_args()
    return args, parser


args, parser = parse_args()

if not args.build and not args.push and not args.registry:
    print("You need to specify either --build or --push")
    print("Example: ./make.py --build open --lightweight --tag 2021.03 --verbose")
    print("Example: ./make.py --push open")
    print("Example: ./make.py --build all")


# we need to clone repos from gitlab (private repos) here and not in the dockerfile
# because we need the proper authentification
def clone_repo(repo, git_url, directory=""):
    git_name = git_url.split('/')[-1].replace('.git', '')
    s = ""
    if (directory != ""):
        s += "cd " + directory
    else:
        s += "cd " + repo
    s += " && rm -rf " + git_name #TODO
    s += " && git clone " + git_url
    s += " && cd " + git_name
    s += " && git remote set-url origin " + git_url
    return s

# registry for docker 
registry = "registry.gitlab.inria.fr"

## prep commands (mostly git clones)
inria_xsens = clone_repo("inria_xsens", "git@gitlab.inria.fr:locolearn/inria_xsens.git")
iwbc_teleop =  clone_repo("inria_wbc_teleop", "git@gitlab.inria.fr:locolearn/inria_wbc_teleop.git")
bashrc = "cd base && rm -f .bashrc && cp ../files/.bashrc .bashrc"
icub_bashrc = "cd base-icub && rm -f .bashrc && cp ../files/.bashrc .bashrc"

# for PAL
pal_bashrc = "cd base-pal && rm -f .bashrc && cp ../files/.bashrc .bashrc"
pal_files =  "cd base-pal && rm -rf docker_talos && git clone git@gitlab.inria.fr:locolearn/docker_talos.git && cd docker_talos && git checkout files"
if(args.architecture!= ""):
    pal_files =  "cd base-pal && rm -rf docker_talos && git clone git@gitlab.inria.fr:locolearn/docker_talos.git && cd docker_talos && git checkout fast"
talos_controller = clone_repo("talos_controller", "git@gitlab.inria.fr:locolearn/talos_controller.git", "ros") + " && git checkout devel"
talos_controller_msgs = clone_repo("talos_controller_msgs", "git@gitlab.inria.fr:locolearn/talos_controller_msgs.git",  "ros") # no devel branch here
inria_xsens_ros = clone_repo("inria_xsens_ros", "git@gitlab.inria.fr:locolearn/inria_xsens_ros.git", "ros") # no devel branch here
current_hardware_gazebo = clone_repo("current_hardware_gazebo", "git@gitlab.inria.fr:locolearn/current_hardware_gazebo.git", "ros")
tiago_controller = clone_repo("tiago_controller", "git@gitlab.inria.fr:locolearn/tiago_controller.git", "tiago")
tiago_teleop = clone_repo("tiago_teleop", "git@gitlab.inria.fr:locolearn/tiago_teleop.git", "tiago")

iwbc_icub_controller = clone_repo("iwbc_icub_controller", "git@gitlab.inria.fr:locolearn/iwbc_icub_controller.git")

# specs of each image
images = {
    "open" : ["ubuntu:18.04", # image name
              "registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_open", # where to push
              ["base", "dart", "robot_dart", "pinocchio_tsid", "inria_wbc" ], # base packages
              ["promp", "limbo", "sferes2", "inria_xsens", "inria_wbc_teleop"], # extended packages
              [bashrc],# preparation commands
              [inria_xsens, iwbc_teleop] # extended preparation commands
            ], 
    "nvidia" : ["nvidia/opengl:1.2-glvnd-devel-ubuntu18.04", # image name
                "registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_nvidia", # image tag
                ["base", "dart", "robot_dart", "pinocchio_tsid", "inria_wbc"], # base packages
                ["promp", "limbo", "sferes2", "inria_xsens", "inria_wbc_teleop"], # extended packages
                [bashrc],# preparation commands
                [inria_xsens, iwbc_teleop]# extended preparation commands
                ],
    "pal" : ["registry.gitlab.inria.fr/locolearn/docker_talos/pal:latest", # image name
             "registry.gitlab.inria.fr/locolearn/docker_talos/inria_wbc_pal", # image tag
            ["base-pal", "dart", "robot_dart", "pinocchio_tsid", "inria_wbc", "promp", "inria_xsens", "inria_wbc_teleop","ros","tiago"], # base packages
            ["limbo", "sferes2"], #extended packages
            [pal_bashrc, pal_files, 
            inria_xsens, iwbc_teleop,
            talos_controller, talos_controller_msgs,
            inria_xsens_ros, tiago_controller, current_hardware_gazebo],# preparation commands
            [] # no extended preparation commands
            ],
    "icub" : ["nvidia/cuda:11.4.2-devel-ubuntu20.04", # image name
              "registry.gitlab.inria.fr/locolearn/public/docker_inria_wbc/inria_wbc_icub", # where to push
              ["base-icub", "dart", "robot_dart", "pinocchio_tsid", "inria_wbc", "robotology", "iwbc_icub_controller"], # base packages
              [], # extended packages
              [icub_bashrc, iwbc_icub_controller],# preparation commands
              [] # extended preparation commands
            ], 
}

def run(cmd, dryrun, verbose):
    if verbose or dryrun:
        print(cmd)
    if not dryrun:
        subprocess.run(cmd, shell=True, check=True)



def build_image(image_name, lightweight, use_cache, sub_tag, dryrun, verbose, without_prep_cmds, architecture):
    image = images[image_name]
    base_image = image[0]
    tag_lightweight = "-light" if lightweight else ""
    tag_fast = "-fast" if (architecture != "") else ""
    tag = image[1] + ":" + sub_tag + tag_fast + tag_lightweight
    subdirs = image[2] if lightweight else image[2] + image[3]
    cache = "--no-cache" if use_cache else ""
    cxxflags = ""
    if (architecture != ""):
        cxxflags = "-march=" + architecture
    cmds = [] if without_prep_cmds else image[4] if lightweight else image[4] + image[5]
    for i in cmds:
        run(i, dryrun, verbose)


    run("docker login " + registry, dryrun, verbose)

    for d in subdirs:
        s = "docker build {} --build-arg \"BASE_IMAGE={}\" --build-arg \"CXXFLAGS={}\" --tag=inria_wbc_{}:{} {}/.".format(cache, base_image, cxxflags, image_name, d, d)
        base_image = "inria_wbc_{}:{}".format(image_name, d)
        run(s, dryrun, verbose)

    s = "docker tag {} {}".format(base_image, tag)
    run(s, dryrun, verbose)


def push_image(image_name, lightweight, sub_tag, dryrun, verbose):
    image = images[image_name]
    tag_lightweight = "-light" if lightweight else ""
    tag = image[1] + ":" + sub_tag + tag_lightweight
    run("docker login " + registry, dryrun, verbose)
    run("docker push {}".format(tag), dryrun, verbose)


if args.registry:
    for i in list(images.keys()):
        print(i, "\t:", images[i][1])

if args.build:
    if args.build == "all":
        for i in images:
            for light in [True, False]:
                build_image(i, light, args.no_cache, args.tag, args.dryrun, args.verbose, args.without_prep_cmds, args.architecture)
    else:
        build_image(args.build, args.lightweight, args.no_cache, args.tag, args.dryrun, args.verbose, args.without_prep_cmds, args.architecture)



if args.push:
    if args.push == "all":
        for i in images:
            for light in [True, False]:
                push_image(i, light, args.tag, args.dryrun, args.verbose)
    else:
        push_image(args.push, args.lightweight, args.tag, args.dryrun, args.verbose)
